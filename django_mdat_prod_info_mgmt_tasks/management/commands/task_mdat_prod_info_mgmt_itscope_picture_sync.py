from django.core.management.base import BaseCommand

from django_mdat_prod_info_mgmt_tasks.django_mdat_prod_info_mgmt_tasks.tasks import (
    mdat_prod_info_mgmt_itscope_picture_sync,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        mdat_prod_info_mgmt_itscope_picture_sync()
