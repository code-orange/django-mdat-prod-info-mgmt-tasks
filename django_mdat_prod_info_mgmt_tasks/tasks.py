import csv
from decimal import Decimal
from io import TextIOWrapper
from zipfile import ZipFile

import pysftp
from celery import shared_task
from django.db import IntegrityError

from django_mdat_customer.django_mdat_customer.models import MdatItemPrices
from django_mdat_prod_info_mgmt.django_mdat_prod_info_mgmt.itscope import *
from django_mdat_prod_info_mgmt.django_mdat_prod_info_mgmt.models import *


@shared_task(name="mdat_prod_info_mgmt_itscope_sync")
def mdat_prod_info_mgmt_itscope_sync():
    # fix itscope skip flag
    for item in (
        MdatItems.objects.filter(itscope_skip=True)
        .filter(itscope_id__isnull=False)
        .select_related()
    ):
        item.itscope_skip = False
        item.save()

    # find itscope id by EAN
    for barcode in MdatItemBarcodes.objects.filter(item__itscope_id__isnull=True):
        print(
            "ITSCOPE-Detect, ITEMID: {}, Title: {}".format(
                barcode.item.id, barcode.item.name
            )
        )

        data = itscope_search_item(
            {
                "ean": barcode.barcode,
            }
        )

        if data is False:
            continue

        barcode.item.itscope_id = data["product"][0]["puid"]
        barcode.item.save()

    # fill EAN for itscope id
    for item in (
        MdatItems.objects.filter(itscope_skip=False)
        .filter(itscope_id__isnull=False)
        .filter(mdatitembarcodes__isnull=True)
        .order_by("-id")
    ):
        # workaround: sql server gone away
        item.refresh_from_db()

        print("EAN-Detect, ITEMID: {}, Title: {}".format(item.id, item.name))

        data = itscope_fetch_item(item.itscope_id)

        if data is False:
            continue

        if "ean" not in data["product"][0]:
            continue

        new_barcode = MdatItemBarcodes(item=item, barcode=data["product"][0]["ean"])

        try:
            new_barcode.save()
        except IntegrityError:
            continue

    return


@shared_task(name="mdat_prod_info_mgmt_pricelist_also_sync")
def mdat_prod_info_mgmt_pricelist_also_sync():
    # disable host key validation
    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None

    sftp_connection = pysftp.Connection(
        host=settings.PROD_INFO_MGMT_PRICELIST_ALSO_SERVER,
        username=settings.PROD_INFO_MGMT_PRICELIST_ALSO_USERNAME,
        password=settings.PROD_INFO_MGMT_PRICELIST_ALSO_PASSWORD,
        cnopts=cnopts,
    )

    pricelist_file = "/" + settings.PROD_INFO_MGMT_PRICELIST_ALSO_FILENAME

    pricelist_zip = BytesIO()

    # fetch pricelist
    sftp_connection.getfo(pricelist_file, pricelist_zip)

    pricelist_zip.seek(0)

    zip_file = ZipFile(pricelist_zip)

    for file in zip_file.namelist():
        if file.endswith(".txt"):
            with zip_file.open(file, "r") as csv_file:
                csv_reader = csv.reader(
                    TextIOWrapper(csv_file, encoding="cp1252"), delimiter="\t"
                )

                for row in csv_reader:
                    ean = row[3]
                    name = row[4]
                    price = Decimal(row[6]) * Decimal(1.2)

                    if ean is None or name is None:
                        continue

                    if ean == "" or name == "":
                        continue

                    if len(ean) != 13:
                        continue

                    print('ALSO: Article "{}" with EAN {}.'.format(name, ean))

                    try:
                        barcode = MdatItemBarcodes.objects.get(barcode=ean)
                    except MdatItemBarcodes.DoesNotExist:
                        print(
                            'ALSO: Article "{}" with EAN {} is new.'.format(name, ean)
                        )

                        new_item = MdatItems(
                            linked_to_id=1,
                            created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                            itscope_skip=False,
                            name=name,
                        )

                        new_item.save(force_insert=True)

                        new_barcode = MdatItemBarcodes(
                            item=new_item,
                            barcode=ean,
                        )
                        new_barcode.save()

                        new_price = MdatItemPrices(
                            item=new_item,
                            price=price,
                        )
                        new_price.save()

                    except MdatItemBarcodes.MultipleObjectsReturned:
                        continue
                    else:
                        print(
                            'ALSO: Article "{}" with EAN {} is known.'.format(name, ean)
                        )

    return


@shared_task(name="mdat_prod_info_mgmt_itscope_export_import")
def mdat_prod_info_mgmt_itscope_export_import():
    for profile in MdatItemItscopeExImports.objects.all().order_by("-id"):
        mdat_prod_info_mgmt_itscope_export_import_single.delay(profile.export_code)

    return


@shared_task(name="mdat_prod_info_mgmt_itscope_export_import_single")
def mdat_prod_info_mgmt_itscope_export_import_single(export_profile: str):
    csv.field_size_limit(2147483647)

    print("Fetching Export-ID {}".format(export_profile))
    export_file = itscope_fetch_product_export(export_profile)

    if export_file is False:
        return False

    export_content = BytesIO(export_file)

    zip_file = ZipFile(export_content)

    with zip_file.open("product.csv", "r") as product_csv:
        csv_reader = csv.DictReader(
            TextIOWrapper(product_csv, encoding="utf-8"), delimiter="\t"
        )

        for row in csv_reader:
            print("Processing IT-Scope-ID {}".format(row["puid"]))

            matches = MdatItems.objects.filter(
                itscope_id=row["puid"], created_by_id=settings.MDAT_ROOT_CUSTOMER_ID
            )

            item_name = row["productName"][:250]

            if len(matches) <= 0:
                new_item = MdatItems(
                    linked_to_id=1,
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                    itscope_id=row["puid"],
                    itscope_skip=False,
                    name=item_name,
                )
                new_item.save(force_insert=True)

                matches = [new_item]

            # set item specs
            for match in matches:
                # set other ids
                if row["icecatId"] is not None:
                    if row["icecatId"] != "":
                        match.icecat_id = row["icecatId"]

                if row["cnetId"] is not None:
                    if row["cnetId"] != "":
                        match.cnet_id = row["cnetId"]

                if row["bechlemId"] is not None:
                    if row["bechlemId"] != "":
                        match.bechlem_id = row["bechlemId"]

                # update price
                if row["price"]:
                    match.update_price(Decimal(row["price"]) * Decimal(1.5))

                # set title
                match.name = item_name

                # set descriptions
                match.description_text = row["longDescription"]

                # set cache
                itscope_cache = MdatItemCacheItscope(
                    item=match,
                    content=row,
                )
                itscope_cache.save()

                # set category
                try:
                    match_category = MdatItemCategoryToItem(
                        item=match,
                        category=MdatItemCategory.objects.get(
                            itscope_id=row["productTypeId"]
                        ),
                    )
                    match_category.save()
                except:
                    try:
                        match_category = MdatItemCategoryToItem(
                            item=match,
                            category=MdatItemCategory.objects.get(
                                itscope_id=row["productTypeGroupId"]
                            ),
                        )
                        match_category.save()
                    except:
                        pass

                # set manufacturer
                try:
                    match_manufacturer = MdatItemManufacturerToItem(
                        item=match,
                        manufacturer=MdatItemManufacturer.objects.get(
                            itscope_id=row["manufacturerId"]
                        ),
                    )
                    match_manufacturer.save()
                except MdatItemManufacturer.DoesNotExist:
                    pass

                # set SKU
                if row["manufacturerSKU"] is not None:
                    if row["manufacturerSKU"] != "":
                        match.manufacturer_sku = row["manufacturerSKU"]

                if row["ean"] is not None:
                    if row["ean"] != "":
                        try:
                            new_barcode = MdatItemBarcodes(
                                item=match,
                                barcode=row["ean"],
                            )
                            new_barcode.save()
                        except IntegrityError:
                            pass

                if row["currencyCode"] == "EUR":
                    try:
                        new_price = MdatItemPrices(
                            item=match,
                            price=Decimal(row["price"]) * Decimal(1.4),
                        )
                        new_price.save()
                    except IntegrityError:
                        pass

                match.save()

    return True


@shared_task(name="mdat_prod_info_mgmt_itscope_datasheet_pdf_sync")
def mdat_prod_info_mgmt_itscope_datasheet_pdf_sync():
    items_missing_datasheet = (
        MdatItems.objects.filter(
            mdatitemdatasheets__isnull=True,
        )
        .filter(
            mdatitemcacheitscope__isnull=False,
        )
        .order_by("-id")
    )

    for item in items_missing_datasheet[:100000]:
        mdat_prod_info_mgmt_itscope_datasheet_pdf_sync_single.delay(item.id)

    return


@shared_task(name="mdat_prod_info_mgmt_itscope_datasheet_pdf_sync_single")
def mdat_prod_info_mgmt_itscope_datasheet_pdf_sync_single(item_id: int):
    item = MdatItems.objects.get(id=item_id)

    content = None

    # if 'manufacturerDatasheet' in item.mdatitemcacheitscope.content:
    #     if item.mdatitemcacheitscope.content['manufacturerDatasheet'] != '':
    #         pdf_file_request = requests.get(item.mdatitemcacheitscope.content['manufacturerDatasheet'])
    #
    #         if pdf_file_request.status_code == 200:
    #             content = pdf_file_request.content

    if content is None and "standardPdfDatasheet" in item.mdatitemcacheitscope.content:
        if item.mdatitemcacheitscope.content["standardPdfDatasheet"] != "":
            pdf_file_request = requests.get(
                item.mdatitemcacheitscope.content["standardPdfDatasheet"]
            )

            if pdf_file_request.status_code == 200:
                content = pdf_file_request.content

    if content is not None:
        new_datasheet = MdatItemDataSheets.objects.create(
            item=item,
            content=content,
        )

    return


@shared_task(name="mdat_prod_info_mgmt_itscope_datasheet_html_sync")
def mdat_prod_info_mgmt_itscope_datasheet_html_sync():
    items_missing_datasheet = (
        MdatItems.objects.filter(
            mdatitemdatasheets__isnull=True,
        )
        .filter(
            mdatitemcacheitscope__isnull=False,
        )
        .order_by("-id")
    )

    for item in items_missing_datasheet[:100000]:
        mdat_prod_info_mgmt_itscope_datasheet_html_sync_single.delay(item.id)

    return


@shared_task(name="mdat_prod_info_mgmt_itscope_datasheet_html_sync_single")
def mdat_prod_info_mgmt_itscope_datasheet_html_sync_single(item_id: int):
    item = MdatItems.objects.get(id=item_id)

    if "standardHtmlDatasheet" in item.mdatitemcacheitscope.content:
        if item.mdatitemcacheitscope.content["standardHtmlDatasheet"] != "":
            html_file_request = requests.get(
                item.mdatitemcacheitscope.content["standardHtmlDatasheet"]
            )

            if html_file_request.status_code == 200:
                item.description_html = html_file_request.text
                item.save()

    return


@shared_task(name="mdat_prod_info_mgmt_itscope_picture_sync")
def mdat_prod_info_mgmt_itscope_picture_sync():
    items_missing_picture = (
        MdatItems.objects.filter(
            mdatitempictures__isnull=True,
        )
        .filter(
            mdatitemcacheitscope__isnull=False,
        )
        .order_by("-id")
    )

    for item in items_missing_picture:
        mdat_prod_info_mgmt_itscope_picture_sync_single.delay(item.id)

    return


@shared_task(name="mdat_prod_info_mgmt_itscope_picture_sync_single")
def mdat_prod_info_mgmt_itscope_picture_sync_single(item_id: int):
    item = MdatItems.objects.get(id=item_id)

    content = None

    if "imageHighRes1" in item.mdatitemcacheitscope.content:
        if item.mdatitemcacheitscope.content["imageHighRes1"] != "":
            picture_file_request = requests.get(
                item.mdatitemcacheitscope.content["imageHighRes1"]
            )

            if picture_file_request.status_code == 200:
                content = picture_file_request.content

    if content is not None:
        new_picture = MdatItemPictures.objects.create(
            item=item,
            content=content,
        )

    return


@shared_task(name="mdat_prod_info_mgmt_itscope_category_sync")
def mdat_prod_info_mgmt_itscope_category_sync():
    categories = itscope_fetch_categories()["productType"]

    for category in categories:
        try:
            product_type_group = MdatItemCategory.objects.get(
                itscope_id=category["productTypeGroup"]["id"]
            )
        except MdatItemCategory.DoesNotExist:
            product_type_group = MdatItemCategory(
                itscope_id=category["productTypeGroup"]["id"],
            )

        product_type_group.name = category["productTypeGroup"]["name"]

        product_type_group.save()

        try:
            product_type = MdatItemCategory.objects.get(itscope_id=category["id"])
        except MdatItemCategory.DoesNotExist:
            product_type = MdatItemCategory(
                itscope_id=category["id"],
            )

        product_type.name = category["name"]
        product_type.parent_category = product_type_group

        product_type.save()

    return


@shared_task(name="mdat_prod_info_mgmt_itscope_manufacturer_sync")
def mdat_prod_info_mgmt_itscope_manufacturer_sync():
    manufacturers = itscope_fetch_manufacturers()["company"]

    for manufacturer in manufacturers:
        try:
            manufacturer_obj = MdatItemManufacturer.objects.get(
                itscope_id=manufacturer["manufacturer"]["id"]
            )
        except MdatItemManufacturer.DoesNotExist:
            manufacturer_obj = MdatItemManufacturer(
                itscope_id=manufacturer["manufacturer"]["id"],
            )

        manufacturer_obj.name = manufacturer["manufacturer"]["name"]

        manufacturer_obj.save()

    return


@shared_task(name="mdat_prod_info_mgmt_item_text_fill")
def mdat_prod_info_mgmt_item_text_fill():
    items_missing_name = MdatItems.objects.filter(translations__name__isnull=True)

    for item_missing_name in items_missing_name:
        if item_missing_name.description_text is not None:
            item_missing_name.name = item_missing_name.description_text
            item_missing_name.save()

    items_missing_description_text = MdatItems.objects.filter(
        translations__description_text__isnull=True
    )

    for item_missing_description_text in items_missing_description_text:
        if item_missing_description_text.name is not None:
            item_missing_description_text.description_text = (
                item_missing_description_text.name
            )
            item_missing_description_text.save()

    return
